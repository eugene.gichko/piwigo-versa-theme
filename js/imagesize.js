function changeImgSrc( url, typeSave, typeMap, typeSize )
{
	var imgObj = document.getElementById("theMainImage");
	if (imgObj)
	{
		imgObj.removeAttribute("width");
		imgObj.removeAttribute("height");
		imgObj.src = url;
		imgObj.style.maxWidth = typeSize.substring(0,typeSize.lastIndexOf("x")-1)+"px";
		imgObj.style.maxHeight = typeSize.substring(typeSize.lastIndexOf("x")+1)+"px";
	}
	var imgBack = document.getElementById("theBackImage");
	if (imgBack)
	{
		imgBack.style.backgroundImage = "url(" + url + ")";
	}
	var imgHead = document.getElementById("imageHeaderBar");
	if (imgHead)
	{
		imgHead.style.maxWidth = typeSize.substring(0,typeSize.lastIndexOf("x")-1)+"px";
	}
	var imgInfo = document.getElementById("InfosAndComments");
	if (imgInfo)
	{
		imgInfo.style.maxWidth = typeSize.substring(0,typeSize.lastIndexOf("x")-1)+"px";
	}
	jQuery('#derivativeSwitchBox .switchCheck').css('visibility','hidden');
	jQuery('#derivativeChecked'+typeMap).css('visibility','visible');
	document.cookie = 'picture_deriv='+typeSave+';path={/literal}{$COOKIE_PATH}{literal}';
}
(SwitchBox=window.SwitchBox||[]).push("#derivativeSwitchLink", "#derivativeSwitchBox");
