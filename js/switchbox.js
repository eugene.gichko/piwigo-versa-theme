(function () {
	var sbFunc = function(link, box) {
		$(link).click(function() {
			$(box).toggle();
			( $(box).is(":hidden") ) ? $(this).parent().removeClass( "switchOpen" ) : $(this).parent().addClass( "switchOpen" );
			return false;
		});
		$(box).on("mouseleave click", function() {
			$(this).hide();
			$(this).parent().removeClass( "switchOpen" );
		});
	};

	if (window.SwitchBox) {
		for (var i=0; i<SwitchBox.length; i+=2)
			sbFunc(SwitchBox[i], SwitchBox[i+1]);
	}

	window.SwitchBox = {
		push: sbFunc
	}
})();
