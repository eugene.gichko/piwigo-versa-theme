$(function() {

	// Fix image height on mobile browsers with misleading CSS vh units
	setTimeout( function() {  
	if( window.innerHeight < $('#theBackImage').height() ) {
		$("#theBackImage").css( "height", window.innerHeight + "px" );
	} }, 200);

	// Add gesture events to the image
	$("#theBackImage").on( 'tapstart', function( e, touch ) {
		// Initial coordinates of the swipe gesture
		var initpos = touch.position
		// Activate the tapmove event 
		startBackImgSlide( initpos );
	});
});

function startBackImgSlide( initpos ) {
	
	// Default to no transition time and the hand cursor
	$("#theBackImage").css( "transition-duration", "0s" );
	$("#theBackImage").css( "cursor", "grabbing" );
	
	// During the swipe displace the image and after a 38% threshold trigger the image change
	// Vertical scrolling is blocked unless the gesture is strongly vertical
	$("#theBackImage").on( 'tapmove', function( e, touch ) {
		
		var pxDispX = touch.position.x - initpos.x;
		var pcDispX = pxDispX / $(this).width();
		( Math.abs( pcDispX ) > 0.38 ) ? backImgSlideOff( pcDispX ) : $(this).css( "transform", "translateX(" + pxDispX + "px)" );
		
		var pxDispY = touch.position.y - initpos.y;
		if( Math.abs( pxDispY ) < Math.abs( 2*pxDispX ) ) e.preventDefault();
	});
	
	// Reset of image displacement if gesture finishes before swipe reaches the threshold
	$("#theBackImage").on( 'tapend', function( e, touch ) {
		$(this).off( 'tapmove' );
		$(this).css( "transition-duration", "0.5s" );
		$(this).css( "transform", "translateX(0px)" );
		$(this).css( "opacity", "1" );
		$(this).css( "cursor","grab" );
		$(this).off( 'tapend' );
	});
}

function backImgSlideOff( pcEndX ) {
	// Unbind events and follow the corresponding link
	$("#theBackImage").off( 'tapmove' );
	$("#theBackImage").off( 'tapend' );
	$("#theBackImage").css( "cursor", "" );
	// Trigger slide off animation
	$("#theBackImage").css( "transition-duration", "0.5s" );
	$("#theBackImage").css( "transform", "translateX(" + (pcEndX*200) + "%)" );
	$("#theBackImage").css( "opacity", "0" );
	// Follow the corresponding link
	if( pcEndX > 0 ) {
		setTimeout( function() { 
			$('#linkPrev')[0].click(); 
		}, 500 );
	} else {
		setTimeout( function() { 
			$('#linkNext')[0].click(); 
		}, 500 );
	}
}

