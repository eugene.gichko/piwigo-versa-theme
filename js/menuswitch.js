var session_storage = window.sessionStorage || {};

if ( session_storage['page-menu'] == 'hidden' ) $("#menubar").hide(0);

function hideMenu() {
	$("#menubar").removeClass( "open" ).addClass( "close" );
	$("#menuSwitcher").removeClass( "open" ).addClass( "close" );
	session_storage['page-menu'] = 'hidden';
}

function showMenu() {
	$("#menubar").removeClass( "close" ).addClass( "open" );
	$("#menuSwitcher").removeClass( "close" ).addClass( "open" );
	session_storage['page-menu'] = 'visible';
}

$(function(){
	$("#menuSwitcher").click(function(e){
		( $("#menubar").is(":hidden") ) ? showMenu() : hideMenu();
		e.preventDefault();
	});
});
