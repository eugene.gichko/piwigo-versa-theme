<?php
/*
Theme Name: Versa
Version: 0.7
Description: Responsive dark theme for modern browsers that maximises the visualization of photographs.
Theme URI: http://piwigo.org/ext/extension_view.php?eid=853 
Author: lexming
Author URI: http://piwigo.org/forum/profile.php?id=23261
*/
$themeconf = array(
  'name'  => 'Versa',
  'parent' => 'default',
  'load_parent_css' => false,
);

// initially show metadata
if ( ! isset($_GET['metadata']) )
{
	pwg_set_session_var('show_metadata', true);
}

?>
