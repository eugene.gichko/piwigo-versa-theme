<div id="mainBox">

{if isset($MENUBAR)}{$MENUBAR}{/if}
{combine_script id='menu.switch' load='footer' require='jquery' path='themes/Versa/js/menuswitch.js'}

<div id="contentBox">
<div id="content" class="content">

{if isset($errors) or isset($infos)}
<div class="content messages">
{include file='infos_errors.tpl'}
</div>
{/if}

<div class="titrePage">
	<ul class="categoryActions">
	</ul>
	<h2><a href="{$U_HOME}">{'Home'|@translate}</a>{$LEVEL_SEPARATOR}{'Profile'|@translate}</h2>
</div>

{$PROFILE_CONTENT}

</div> <!-- content -->
</div> <!-- contentBox --> 
</div> <!-- mainBox --> 
