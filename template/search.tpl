<div id="mainBox">

{* Example of resizeable *}
{*
{include file='include/resize.inc.tpl'}
*}

{combine_script id='jquery.selectize' load='footer' path='themes/default/js/plugins/selectize.min.js'}
{combine_css id='jquery.selectize' path="themes/default/js/plugins/selectize.{$themeconf.colorscheme}.css"}

{footer_script}
jQuery(document).ready(function() {
  jQuery("#authors, #tags, #categories").each(function() {
    jQuery(this).selectize({
      plugins: ['remove_button'],
      maxOptions:jQuery(this).find("option").length
    });
  })
});
{/footer_script}

{if isset($MENUBAR)}{$MENUBAR}{/if}
{combine_script id='menu.switch' load='footer' require='jquery' path='themes/Versa/js/menuswitch.js'}

<div id="contentBox">

{if isset($errors) or isset($infos)}
<div class="content messages">
{include file='infos_errors.tpl'}
</div>
{/if}

<div id="content" class="content">

	<div class="titrePage">
		<ul class="categoryActions">
			{combine_script id='core.scripts' load='async' path='themes/default/js/scripts.js'}
			<li><a href="{$U_HELP}" onclick="popuphelp(this.href); return false;" title="{'Help'|@translate}" class="pwg-state-default pwg-button">
				<span class="pwg-icon pwg-icon-help"></span><span class="pwg-button-text">{'Help'|@translate}</span>
			</a></li>
		</ul>
		<h2><a href="{$U_HOME}">{'Home'|@translate}</a>{$LEVEL_SEPARATOR}{'Search'|@translate}</h2>
	</div>

<form class="filter" method="post" name="search" action="{$F_SEARCH_ACTION}">
<fieldset>
  <legend>{'Search for words'|@translate}</legend>
  <input type="text" size="24" name="search_allwords">
  <ul>
    <li>
      <input id="srchAllTerms" type="radio" name="mode" value="AND" checked="checked">
      <label class="radioLab" for="srchAllTerms">{'Search for all terms'|@translate}</label>
    </li>
    <li>
      <input id="srchAnyTerm" type="radio" name="mode" value="OR">
      <label class="radioLab" for="srchAnyTerm">{'Search for any term'|@translate}</label>
    </li>
  </ul>

  <strong>{'Apply on properties'|@translate}</strong>
  <ul>
    <li>
      <input id="srchPhoto" type="checkbox" name="fields[]" value="name" checked="checked">
      <label class="checkLab" for="srchPhoto">{'Photo title'|@translate}</label>
    </li>
    <li>
      <input id="srchPhotoDesc" type="checkbox" name="fields[]" value="comment" checked="checked">
      <label class="checkLab" for="srchPhotoDesc">{'Photo description'|@translate}</label>
    </li>
    <li>
      <input id="srchFile" type="checkbox" name="fields[]" value="name" checked="checked">
      <label class="checkLab" for="srchFile">{'File name'|@translate}</label>
    </li>
{if isset($TAGS)}
    <li>
      <input id="srchTag" type="checkbox" name="search_in_tags" value="tags">
      <label class="checkLab" for="srchTag">{'Tags'|@translate}</label>
    </li>
{/if}
  </ul>

</fieldset>

{if count($AUTHORS)>=1}
<fieldset>
  <legend>{'Search for Author'|@translate}</legend>
  <p>
    <select id="authors" placeholder="{'Type in a search term'|@translate}" name="authors[]" multiple>
{foreach from=$AUTHORS item=author}
      <option value="{$author.author|strip_tags:false|escape:html}">{$author.author|strip_tags:false} ({$author.counter|@translate_dec:'%d photo':'%d photos'})</option>
{/foreach}
    </select>
  </p>
</fieldset>
{/if}

{if isset($TAGS)}
<fieldset>
  <legend>{'Search tags'|@translate}</legend>
  <select id="tags" placeholder="{'Type in a search term'|@translate}" name="tags[]" multiple>
{foreach from=$TAGS item=tag}
    <option value="{$tag.id}">{$tag.name} ({$tag.counter|@translate_dec:'%d photo':'%d photos'})</option>
{/foreach}
  </select>
  <ul>
    <li>
      <input id="srchAllTags" type="radio" name="tag_mode" value="AND" checked="checked">
      <label class="radioLab" for="srchAllTags">{'All tags'|@translate}</label>
    </li>
    <li>
      <input id="srchAnyTags" type="radio" name="tag_mode" value="OR">
      <label class="radioLab" for="srchAnyTags">{'Any tags'|@translate}</label>
    </li>
  </ul>
</fieldset>
{/if}

<fieldset>
  <legend>{'Search by date'|@translate}</legend>
  <p>
    <select id="start_day" name="start_day">
        <option value="0">--</option>
      {section name=day start=1 loop=32}
        <option value="{$smarty.section.day.index}" {if $smarty.section.day.index==$START_DAY_SELECTED}selected="selected"{/if}>{$smarty.section.day.index}</option>
      {/section}
    </select>
    <select id="start_month" name="start_month">
      {html_options options=$month_list selected=$START_MONTH_SELECTED}
    </select>
    <input id="start_year" name="start_year" type="text" size="4" maxlength="4" >
    <input id="start_linked_date" name="start_linked_date" type="hidden" size="10" disabled="disabled">
    <a class="date_today" href="#" onClick="document.search.start_day.value={$smarty.now|date_format:"%d"};document.search.start_month.value={$smarty.now|date_format:"%m"};document.search.start_year.value={$smarty.now|date_format:"%Y"};return false;">[{'today'|@translate}]</a>
  </p>
  <p class="date_arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></p>
  <p>
    <select id="end_day" name="end_day">
        <option value="0">--</option>
      {section name=day start=1 loop=32}
        <option value="{$smarty.section.day.index}" {if $smarty.section.day.index==$END_DAY_SELECTED}selected="selected"{/if}>{$smarty.section.day.index}</option>
      {/section}
    </select>
    <select id="end_month" name="end_month">
      {html_options options=$month_list selected=$END_MONTH_SELECTED}
    </select>
    <input id="end_year" name="end_year" type="text" size="4" maxlength="4" >
    <input id="end_linked_date" name="end_linked_date" type="hidden" size="10" disabled="disabled">
    <a class="date_today" href="#" onClick="document.search.end_day.value={$smarty.now|date_format:"%d"};document.search.end_month.value={$smarty.now|date_format:"%m"};document.search.end_year.value={$smarty.now|date_format:"%Y"};return false;">[{'today'|@translate}]</a>
  </p>
  <ul>
    <li>
      <input id="srchCreation" type="radio" name="date_type" value="date_creation" checked="checked">
      <label class="radioLab" for="srchCreation">{'Creation date'|@translate}</label>
    </li>
    <li>
      <input id="srchPost" type="radio" name="date_type" value="date_available">
      <label class="radioLab" for="srchPost">{'Post date'|@translate}</label>
    </li>
  </ul>
</fieldset>

<fieldset>
  <legend>{'Search in albums'|@translate}</legend>
  <p>
    <select id="categories" name="cat[]" multiple>
      {html_options options=$category_options selected=$category_options_selected}
    </select>
  </p>
  <ul>
    <li>
      <input id="srchSubCat" type="checkbox" name="subcats-included" value="1" checked="checked">
      <label class="checkLab" for="srchSubCat">{'Search in sub-albums'|@translate}</label>
    </li>
  </ul>
</fieldset>
<p>
  <input type="submit" name="submit" value="{'Search'|@translate}">
  <input type="reset" value="{'Reset'|@translate}">
</p>
</form>

<script type="text/javascript"><!--
document.search.search_allwords.focus();
//--></script>

</div> <!-- content -->
</div> <!-- contentBox --> 
</div> <!-- mainBox --> 
