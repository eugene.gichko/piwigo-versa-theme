<div id="slideshow">
	<div id="content">
	<div id="theImage">
	  {$ELEMENT_CONTENT}
	</div>
	</div>
	
	<div id="imageHeaderBar">
	  <div class="browsePath">
			<h2 class="showtitle">{$current.TITLE}</h2>
	  </div>
	</div>

	<ul id="imageToolBar">
	  {include file='picture_nav_buttons.tpl'|@get_extent:'picture_nav_buttons'}
	  <li class="imageNumber">{$PHOTO}</li>
	</ul>
</div>
