<div id="mainBox">
{if isset($MENUBAR)}{$MENUBAR}{/if}
{combine_script id='menu.switch' load='footer' require='jquery' path='themes/Versa/js/menuswitch.js'}

<div id="contentBox">

{if isset($errors) or isset($infos)}
<div class="content messages">
{include file='infos_errors.tpl'}
</div>
{/if}

<div class="content">

<div class="titrePage">
	<ul class="categoryActions">
{if $display_mode == 'letters'}
		<li><a href="{$U_CLOUD}" title="{'show tag cloud'|@translate}" class="pwg-state-default pwg-button">
			<span class="pwg-icon pwg-icon-cloud"></span><span class="pwg-button-text">{'cloud'|@translate}</span>
		</a></li>
{/if}
{if $display_mode == 'cloud'}
		<li><a href="{$U_LETTERS}" title="{'group by letters'|@translate}" class="pwg-state-default pwg-button" rel="nofollow">
			<span class="pwg-icon pwg-icon-letters"></span><span class="pwg-button-text">{'letters'|@translate}</span>
		</a></li>
{/if}
	</ul>
	<h2><a href="{$U_HOME}">{'Home'|@translate}</a>{$LEVEL_SEPARATOR}{'Tags'|@translate}</h2>
</div>

{if $display_mode == 'cloud' and isset($tags)}
<div id="fullTagCloud">
	{foreach from=$tags item=tag}
	<span><a href="{$tag.URL}" class="tagLevel{$tag.level}" title="{$tag.counter|@translate_dec:'%d photo':'%d photos'}">{$tag.name}</a></span>
	{/foreach}
</div>
{/if}

{if $display_mode == 'letters' and isset($letters)}
<div id="tagLetterList">
	{foreach from=$letters item=letter}
	<dl class="tagLetter">
		<dt class="tagLetterLegend">{$letter.TITLE}</dt>
		<dd class="tagLetterContent"><ul>
		{foreach from=$letter.tags item=tag}
		<li class="tagLine">
			<a href="{$tag.URL}" title="{$tag.name}">{$tag.name}</a>
			<span class="nbEntries">{$tag.counter|@translate_dec:'%d photo':'%d photos'}</span>
		</li>
		{/foreach}
		</ul></dd>
	</dl>
	{/foreach}
</div>
{/if}

</div> <!-- content -->
</div> <!-- contentBox --> 
</div> <!-- mainBox --> 
