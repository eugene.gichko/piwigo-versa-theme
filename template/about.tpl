<div id="mainBox">
{if isset($MENUBAR)}{$MENUBAR}{/if}
{combine_script id='menu.switch' load='footer' require='jquery' path='themes/Versa/js/menuswitch.js'}

<div id="contentBox">

<div id="content" class="content">
  <div class="titrePage">
    <ul class="categoryActions">
    </ul>
    <h2><a href="{$U_HOME}">{'Home'|@translate}</a>{$LEVEL_SEPARATOR}{'About'|@translate}</h2>
  </div>
  
  {include file='infos_errors.tpl'}
  
  <div id="piwigoAbout">
  {$ABOUT_MESSAGE}
  {if isset($THEME_ABOUT) }
  <ul>
   <li>{$THEME_ABOUT}</li>
  </ul>
  {/if}
  {if not empty($about_msgs)}
    {foreach from=$about_msgs item=elt}
    {$elt}
    {/foreach}
  {/if}
  </div>
</div> <!-- content -->
</div> <!-- contentBox --> 
</div> <!-- mainBox --> 
