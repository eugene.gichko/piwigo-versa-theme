{footer_script}
  var error_icon = "{$ROOT_URL}{$themeconf.icon_dir}/errors_small.png", max_requests = {$maxRequests};
{/footer_script}
<div class="loader"><img src="{$ROOT_URL}{$themeconf.img_dir}/ajax_loader.gif"></div>
<ul class="thumbnailsCategories">
{foreach from=$category_thumbnails item=cat name=cat_loop}
{assign var=derivative value=$pwg->derivative($derivative_params, $cat.representative.src_image)}
{if !$derivative->is_cached()}
{combine_script id='jquery.ajaxmanager' path='themes/default/js/plugins/jquery.ajaxmanager.js' load='footer'}
{combine_script id='thumbnails.loader' path='themes/default/js/thumbnails.loader.js' require='jquery.ajaxmanager' load='footer'}
{/if}
  <li>
	<div>
	<h3>
		<a href="{$cat.URL}">{$cat.NAME}</a>
		{if !empty($cat.icon_ts)}
		&nbsp;<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
		{/if}
	</h3>
	{if isset($cat.INFO_DATES) }
	<p class="dates">{$cat.INFO_DATES}</p>
	{/if}
	<p class="Nb_images">{$cat.CAPTION_NB_IMAGES}</p>
	{if not empty($cat.DESCRIPTION)}
	<p>{$cat.DESCRIPTION}</p>
	{/if}
	</div>
	<a href="{$cat.URL}">
	<img class="coverCategory" {if $derivative->is_cached()}src="{$derivative->get_url()}"{else}src="{$ROOT_URL}{$themeconf.icon_dir}/img_small.png" data-src="{$derivative->get_url()}"{/if} alt="{$cat.TN_ALT}" title="{$cat.NAME|@replace:'"':' '|@strip_tags:false} - {'display this album'|@translate}">
	</a>
  </li>
{/foreach}
</ul>
