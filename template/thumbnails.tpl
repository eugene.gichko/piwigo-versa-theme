{if !empty($thumbnails)}{strip}
{html_style}
{*Set some sizes according to maximum thumbnail width and height*}
.thumbnails LI{ldelim}
	max-width: {$derivative_params->max_width()}px;
	flex-basis: {$derivative_params->max_width()/2}px;
{rdelim}
{/html_style}
{footer_script}
  var error_icon = "{$ROOT_URL}{$themeconf.icon_dir}/errors_small.png", max_requests = {$maxRequests};
{/footer_script}
{foreach from=$thumbnails item=thumbnail}
{assign var=derivative value=$pwg->derivative($derivative_params, $thumbnail.src_image)}
{if !$derivative->is_cached()}
{combine_script id='jquery.ajaxmanager' path='themes/default/js/plugins/jquery.ajaxmanager.js' load='footer'}
{combine_script id='thumbnails.loader' path='themes/default/js/thumbnails.loader.js' require='jquery.ajaxmanager' load='footer'}
{/if}
<li>
	<a href="{$thumbnail.URL}#theImage">
		<img class="thumbPhoto" {if $derivative->is_cached()}src="{$derivative->get_url()}"{else}src="{$ROOT_URL}{$themeconf.icon_dir}/img_small.png" data-src="{$derivative->get_url()}"{/if} alt="{$thumbnail.TN_ALT}" title="{$thumbnail.TN_TITLE}">
	</a>
	{if $SHOW_THUMBNAIL_CAPTION }
	<div class="thumbLegend">
		<p class="thumbName"><span>{$thumbnail.NAME}
		{if !empty($thumbnail.icon_ts)}
		&nbsp;<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
		{/if}
		</span></p>
		{if isset($thumbnail.NB_COMMENTS)}
		<p class="{if 0==$thumbnail.NB_COMMENTS}zero {/if}nb-comments"><span>
		{$pwg->l10n_dec('%d comment', '%d comments',$thumbnail.NB_COMMENTS)}
		</span></p>
		{/if}
		{if isset($thumbnail.NB_HITS)}
		<p class="{if 0==$thumbnail.NB_HITS}zero {/if}nb-hits"><span>
		{$pwg->l10n_dec('%d hit', '%d hits',$thumbnail.NB_HITS)}
		</span></p>
		{/if}
	</div>
	{/if}
</li>
{/foreach}{/strip}
{/if}
