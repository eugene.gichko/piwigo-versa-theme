{if isset($comment_derivative_params)}
{strip}{html_style}
.commentElement{ldelim}
	max-width: {$derivative_params->max_width()}px;
	flex-basis: {$derivative_params->max_width()/2}px;
{rdelim}
{/html_style}{/strip}
{footer_script}var error_icon = "{$ROOT_URL}{$themeconf.icon_dir}/errors_small.png";{/footer_script}
<div class="loader"><img src="{$ROOT_URL}{$themeconf.img_dir}/ajax_loader.gif"></div>
{/if}
<ul class="commentsList">
{foreach from=$comments item=comment name=comment_loop}
<li class="commentElement {if $smarty.foreach.comment_loop.index is odd}odd{else}even{/if}">
	{if isset($comment.src_image)}
	{if isset($comment_derivative_params)}
	{assign var=derivative value=$pwg->derivative($comment_derivative_params, $comment.src_image)}
	{else}
	{assign var=derivative value=$pwg->derivative($derivative_params, $comment.src_image)}
	{/if}
	{if !$derivative->is_cached()}
	{combine_script id='jquery.ajaxmanager' path='themes/default/js/plugins/jquery.ajaxmanager.js' load='footer'}
  {combine_script id='thumbnails.loader' path='themes/default/js/thumbnails.loader.js' require='jquery.ajaxmanager' load='footer'}
  {/if}
	<div class="illustration">
		<a href="{$comment.U_PICTURE}">
		<img {if $derivative->is_cached()}src="{$derivative->get_url()}"{else}src="{$ROOT_URL}{$themeconf.icon_dir}/img_small.png" data-src="{$derivative->get_url()}"{/if} alt="{$comment.ALT}">
		</a>
	</div>
	{/if}
	<div class="description">
		{if isset($comment.U_DELETE) or isset($comment.U_VALIDATE) or isset($comment.U_EDIT)}
		<div class="actions">
		{if isset($comment.U_DELETE)}
			<a class="cmtAction" href="{$comment.U_DELETE}" onclick="return confirm('{'Are you sure?'|@translate|@escape:javascript}');">
				<i class="fa fa-eraser" aria-hidden="true"></i> <span>{'Delete'|@translate}</span>
			</a>
		{/if}
		{if isset($comment.U_CANCEL)}
			<a class="cmtAction" href="{$comment.U_CANCEL}">
				<i class="fa fa-times" aria-hidden="true"></i> <span>{'Cancel'|@translate}</span>
			</a>
		{/if}
		{if isset($comment.U_EDIT) and !isset($comment.IN_EDIT)}
			<a class="cmtAction" href="{$comment.U_EDIT}#edit_comment">
				<i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span>{'Edit'|@translate}</span>
			</a>
		{/if}
		{if isset($comment.U_VALIDATE)}
			<a class="cmtAction" ref="{$comment.U_VALIDATE}">
				<i class="fa fa-thumbs-up" aria-hidden="true"></i> <span>{'Validate'|@translate}</span>
			</a>
		{/if}
		</div>
		{/if}

		<div class="commentDate">{$comment.DATE}</div>
		
		<div>
		<span class="commentAuthor">{if $comment.EMAIL}<a href="mailto:{$comment.EMAIL}">{$comment.AUTHOR}:</a>{else}{$comment.AUTHOR}:{/if}</span>
		{if isset($comment.IN_EDIT)}
		<a name="edit_comment"></a>
		<form method="post" action="{$comment.U_EDIT}" id="editComment">
			<p><textarea name="content" id="contenteditid" rows="5">{$comment.CONTENT|@escape}</textarea></p>
			<p><label for="website_url">{'Website'|@translate} :</label></p>
			<p><input type="text" name="website_url" id="website_url" value="{$comment.WEBSITE_URL}"></p>
			<p><input type="hidden" name="key" value="{$comment.KEY}">
				<input type="hidden" name="pwg_token" value="{$comment.PWG_TOKEN}">
				<input type="hidden" name="image_id" value="{$comment.IMAGE_ID|@default:$current.id}">
				<input type="submit" value="{'Submit'|@translate}">
			</p>
		</form>
		{else}
		<span class="commentText">{$comment.CONTENT}</span>
		{/if}
		</div>
	</div>
</li>
{/foreach}
</ul>
