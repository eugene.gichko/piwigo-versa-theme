{if !empty($chronology_navigation_bars)}
{foreach from=$chronology_navigation_bars item=bar}
<div class="calendarBar">
	{if isset($bar.previous)}
		<div class="prevCal">&laquo; <a href="{$bar.previous.URL}">{$bar.previous.LABEL}</a></div>
	{/if}
	<div class="navbarItems">
	{if empty($bar.items)}
		&nbsp;
	{else}
		{foreach from=$bar.items item=item}
		{if !isset($item.URL)}
		<span class="calItem">{$item.LABEL}</span>
		{else}
		<a class="calItem"{if isset($item.NB_IMAGES)} title="{$item.NB_IMAGES|@translate_dec:'%d photo':'%d photos'}"{/if} href="{$item.URL}">{$item.LABEL}</a>
		{/if}
		{/foreach}
	{/if}
	</div>
	{if isset($bar.next)}
		<div class="nextCal"><a href="{$bar.next.URL}">{$bar.next.LABEL}</a> &raquo;</div>
	{/if}
</div>
{/foreach}
{/if}

{if !empty($chronology_calendar.calendar_bars)}
{foreach from=$chronology_calendar.calendar_bars item=bar}
<div class="calendarCalBar">
	<div class="calCalHead"><a href="{$bar.U_HEAD}">{$bar.HEAD_LABEL}</a>  ({$bar.NB_IMAGES})</div>
	<div class="calCalItems">
	{foreach from=$bar.items item=item}
	<span class="calCal{if !isset($item.URL)}Empty{/if}">
	{if isset($item.URL)}
	<a href="{$item.URL}">{$item.LABEL}</a>
	{else}
	{$item.LABEL}
	{/if}
	{if isset($item.NB_IMAGES)}({$item.NB_IMAGES}){/if}
	</span>
	{/foreach}
	</div>
</div>
{/foreach}
{/if}

{if isset($chronology_calendar.month_view)}
<table class="calMonth">
 <thead>
 <tr>
 {foreach from=$chronology_calendar.month_view.wday_labels item=wday}
	<th>{$wday}</th>
 {/foreach}
 </tr>
 </thead>
{html_style}
.calMonth TD, .calMonth .calImg{
	width:{$chronology_calendar.month_view.CELL_WIDTH}px;height:{$chronology_calendar.month_view.CELL_HEIGHT}px
}
{/html_style}
 {foreach from=$chronology_calendar.month_view.weeks item=week}
 <tr>
 	{foreach from=$week item=day}
 	{if !empty($day)}
 		{if isset($day.IMAGE)}
 			<td class="calDayCellFull">
	 			<div class="calBackDate">{$day.DAY}</div><div class="calForeDate">{$day.DAY}</div>
	 			<div class="calImg">
					<a href="{$day.U_IMG_LINK}">
 						<img src="{$day.IMAGE}" alt="{$day.IMAGE_ALT}" title="{$day.NB_ELEMENTS|@translate_dec:'%d photo':'%d photos'}">
					</a>
				</div>
 		{else}
 			<td class="calDayCellEmpty">
	 			<div class="calBackDate">{$day.DAY}</div><div class="calForeDate">{$day.DAY}</div>
 		{/if}
 	{else}{*blank cell first or last row only*}
 		<td>
 	{/if}
 	</td>
 	{/foreach}{*day in week*}
 </tr>
 {/foreach}{*week in month*}
</table>
{/if}

