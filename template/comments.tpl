<div id="mainBox">
{if isset($MENUBAR)}{$MENUBAR}{/if}
{combine_script id='menu.switch' load='footer' require='jquery' path='themes/Versa/js/menuswitch.js'}

<div id="contentBox">

{if isset($errors) or isset($infos)}
<div class="content messages">
{include file='infos_errors.tpl'}
</div>
{/if}

<div id="content" class="content">

<div class="titrePage">
	<ul class="categoryActions">
	</ul>
	<h2><a href="{$U_HOME}">{'Home'|@translate}</a>{$LEVEL_SEPARATOR}{'User comments'|@translate}</h2>
</div>

<form class="filter" action="{$F_ACTION}" method="get">

  <fieldset>
    <legend>{'Filter'|@translate}</legend>
    <p class="cmtFilter">
      <label for="cmtFilKey">{'Keyword'|@translate}</label>
      <input id="cmtFilKey" type="text" name="keyword" value="{$F_KEYWORD}">
    </p>
    <p class="cmtFilter">
      <label for="cmtFilAuthor">{'Author'|@translate}</label>
      <input id="cmtFilAuthor" type="text" name="author" value="{$F_AUTHOR}">
    </p>
    <p class="cmtFilter">
      <label for="cmtFilAlbum">{'Album'|@translate}</label>
      <select id="cmtFilAlbum" name="cat">
        <option value="0">------------</option>
        {html_options options=$categories selected=$categories_selected}
      </select>
    </p>
    <p class="cmtFilter">
      <label for="cmtFilSince">{'Since'|@translate}</label>
      <select id="cmtFilSince" name="since">
        {html_options options=$since_options selected=$since_options_selected}
      </select>
    </p>
  </fieldset>

  <fieldset>
    <legend>{'Display'|@translate}</legend>
    <p class="cmtFilter">
    <label for="cmtFilSort">{'Sort by'|@translate}</label>
    <select id="cmtFilSort" name="sort_by">
      {html_options options=$sort_by_options selected=$sort_by_options_selected}
    </select>
    </p>
    <p class="cmtFilter">
    <label for="cmtFilOrder">{'Sort order'|@translate}</label>
    <select id="cmtFilOrder" name="sort_order">
      {html_options options=$sort_order_options selected=$sort_order_options_selected}
    </select>
    </p>
    <p class="cmtFilter">
    <label for="cmtFilNum">{'Number of items'|@translate}</label>
    <select id="cmtFilNum" name="items_number">
      {html_options options=$item_number_options selected=$item_number_options_selected}
    </select>
    </p>
  </fieldset>

  <p><input type="submit" value="{'Filter and display'|@translate}"></p>

</form>

{if isset($comments)}
<div id="commentsBox">
	{include file='comment_list.tpl' comment_derivative_params=$derivative_params}
</div>
{/if}

{if !empty($navbar) }{include file='navigation_bar.tpl'|@get_extent:'navbar'}{/if}

</div> <!-- content -->
</div> <!-- contentBox --> 
</div> <!-- mainBox --> 

